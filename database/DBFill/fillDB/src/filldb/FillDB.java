/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filldb;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rolan
 */
public class FillDB {

    /**
     * @param args the command line arguments
     */   
    private static int year = 2019;
    private static int month = 3;
    private static int day = 1;
    private static int idBill =1;
    private static int idSale =1;
    private static int numcostumers = 10;
    private static int numofSales = 3;
    private static int maxamount = 20;
    private static Random r = new Random();
    public static void main(String[] args) {
        utils u = new utils();
        int costumday = 1;
        while(costumday <= 90)
        {
            int costumer = r.nextInt(numcostumers)+1;
            int aux = 1;
            while(aux<= costumer)
            {
                int buyer = r.nextInt(utils.clients.size());
                int rucb = utils.clients.get(buyer).ruc;
                
                int sales = r.nextInt(numofSales)+1;
                int act = 1;
                u.insertBill(idBill, rucb, getDate());
                
                System.err.println(idBill + "-" +rucb+" "+getDate());
                while(act <= sales)
                {
                    int prod = r.nextInt(utils.product.size());
                    products p = utils.product.get(prod);
                    int realamount = r.nextInt(maxamount)+1;
                    u.insertSale(idSale, p.idproduct, realamount, realamount*p.priceProduct);
                    u.insertdetail(idBill, idSale);
                    act++;
                    idSale++;
                }
                aux++;
                idBill++;
            }
            day++;
            costumday++;
        }
       
    }
    public static String getDate()
    {
        if(day> 30)
        {
            month++;
            day = 1;
        }
        return ""+year+"-"+month+"-"+day;//+" "+r.nextInt(12)+1+":"+r.nextInt(59)+1+":"+r.nextInt(59)+1
    }
    
    
}
