/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filldb;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rolan
 */
public class utils {
    public static LinkedList<products> product = new LinkedList<>();
    public static LinkedList<client_user> clients = new LinkedList<>();
    public Connection con = Conection.getInstance().connect();

    public utils() {
        fillClients();
        fillProducts();
    }
    public void insertSale(int idSale, int idProduct, int amount, float total)
    {
        try {
            String query = "{ call insertsale(?,?,?,?) }";
            
            ResultSet rs;
            CallableStatement stmt = con.prepareCall(query);
            
            stmt.setInt(1, idSale);
            
            stmt.setInt(2, idProduct);
            
            stmt.setInt(3, amount);
            
            stmt.setFloat(4, total);
            rs = stmt.executeQuery();
       
        } catch (SQLException ex) {
            Logger.getLogger(FillDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void insertdetail(int idbill, int idsale)
    {
        try {
            String query = "{ call insertdetail(?,?) }";
            
            ResultSet rs;
            CallableStatement stmt = con.prepareCall(query);
            
            stmt.setInt(1, idbill);
            
            stmt.setInt(2, idsale);
            
            rs = stmt.executeQuery();
       
        } catch (SQLException ex) {
            Logger.getLogger(FillDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void insertBill(int idbill, int idclient, String dateussed)
    {
        try {
            String query = "{ call insertbill(?,?,?) }";
            
            ResultSet rs;
            CallableStatement stmt = con.prepareCall(query);
            
            stmt.setInt(1, idbill);
            
            stmt.setInt(2, idclient);
            
            stmt.setString(3, dateussed);

            rs = stmt.executeQuery();
       
        } catch (SQLException ex) {
            Logger.getLogger(FillDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void fillProducts() {
        try {
            String query = "{ call getproducts() }";
            ResultSet rs;
            CallableStatement stmt = con.prepareCall(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                products pr = new products(rs.getInt(1), rs.getString(2), rs.getFloat(3));
                product.add(pr);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FillDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillClients() {
        try {
            String query = "{ call getclients() }";
            ResultSet rs;
            CallableStatement stmt = con.prepareCall(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                client_user cu = new client_user(rs.getInt(1), rs.getString(2));
                clients.add(cu);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FillDB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
