
DROP DATABASE IF EXISTS billapp;
CREATE DATABASE billapp;

USE billapp;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bill
-- ----------------------------
DROP TABLE IF EXISTS `bill`;
CREATE TABLE `bill`  (
  `IdBill` int(255) NOT NULL AUTO_INCREMENT,
  `IdClient` int(255) NOT NULL,
  `Dateissued` datetime(0) NOT NULL,
  PRIMARY KEY (`IdBill`) USING BTREE,
  INDEX `fk_client_bill`(`IdClient`) USING BTREE,
  CONSTRAINT `fk_client_bill` FOREIGN KEY (`IdClient`) REFERENCES `client_user` (`RUC`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 3026 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for client_user
-- ----------------------------
DROP TABLE IF EXISTS `client_user`;
CREATE TABLE `client_user`  (
  `RUC` int(255) NOT NULL,
  `Name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`RUC`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for detail
-- ----------------------------
DROP TABLE IF EXISTS `detail`;
CREATE TABLE `detail`  (
  `IdBill` int(255) NOT NULL,
  `Idsales` int(255) NOT NULL,
  INDEX `fk_bill_detail`(`IdBill`) USING BTREE,
  INDEX `fk_sale_datail`(`Idsales`) USING BTREE,
  CONSTRAINT `fk_bill_detail` FOREIGN KEY (`IdBill`) REFERENCES `bill` (`IdBill`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_datail` FOREIGN KEY (`Idsales`) REFERENCES `sale` (`IdSale`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `IdProduct` int(255) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Price` float(255, 1) NOT NULL,
  PRIMARY KEY (`IdProduct`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for sale
-- ----------------------------
DROP TABLE IF EXISTS `sale`;
CREATE TABLE `sale`  (
  `IdSale` int(255) NOT NULL AUTO_INCREMENT,
  `IdProcut` int(255) NOT NULL,
  `Amount` int(255) NOT NULL,
  `Total` float(255, 0) NOT NULL,
  PRIMARY KEY (`IdSale`) USING BTREE,
  INDEX `fk_product_sale`(`IdProcut`) USING BTREE,
  CONSTRAINT `fk_product_sale` FOREIGN KEY (`IdProcut`) REFERENCES `product` (`IdProduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1001 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Procedure structure for getclients
-- ----------------------------
DROP PROCEDURE IF EXISTS `getclients`;
delimiter ;;
CREATE PROCEDURE `getclients`()
BEGIN

 SELECT * FROM client_user;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getproducts
-- ----------------------------
DROP PROCEDURE IF EXISTS `getproducts`;
delimiter ;;
CREATE PROCEDURE `getproducts`()
BEGIN

 SELECT * FROM product;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for gettopclients
-- ----------------------------
DROP PROCEDURE IF EXISTS `gettopclients`;
delimiter ;;
CREATE PROCEDURE `gettopclients`(in `startdate` varchar(255), in `enddate` varchar(255), in rg int)
BEGIN
	SELECT c.RUC , c.`Name`, SUM(s.Total) as Total
	FROM bill b, client_user c, detail d, sale s
	WHERE b.IdBill = d.IdBill AND d.Idsales = s.IdSale AND b.IdClient= c.RUC AND
	b.Dateissued BETWEEN startdate AND enddate
	GROUP BY c.RUC
	ORDER BY Total DESC
LIMIT rg;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for gettopproducts
-- ----------------------------
DROP PROCEDURE IF EXISTS `gettopproducts`;
delimiter ;;
CREATE PROCEDURE `gettopproducts`(in `startdate` varchar(255), in `enddate` varchar(255), in rg int)
BEGIN
 SELECT p.IdProduct, p.`Name`, SUM(s.Amount) as TotalAmount, SUM(s.Total) as Total
FROM sale s, product p, detail d, (SELECT b.IdBill
		FROM bill b
		WHERE b.Dateissued BETWEEN startdate AND enddate) as b
WHERE b.IdBill = d.IdBill AND d.Idsales = s.IdSale AND s.IdProcut = p.IdProduct

GROUP BY p.IdProduct
ORDER BY TotalAmount DESC
LIMIT rg;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insertbill
-- ----------------------------
DROP PROCEDURE IF EXISTS `insertbill`;
delimiter ;;
CREATE PROCEDURE `insertbill`(in `idbill` int,in `idclient` int, in `dateissued` varchar(255))
BEGIN

 INSERT INTO bill VALUES(idbill,idclient,dateissued);

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insertclient
-- ----------------------------
DROP PROCEDURE IF EXISTS `insertclient`;
delimiter ;;
CREATE PROCEDURE `insertclient`(in `ruc` int,in `clientname` varchar(255))
BEGIN

 INSERT INTO client_user VALUES(ruc,clientname);

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insertdetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `insertdetail`;
delimiter ;;
CREATE PROCEDURE `insertdetail`(in `idbill` int,in `idSale` int)
BEGIN

 INSERT INTO detail VALUES(idbill,idSale);

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insertproduct
-- ----------------------------
DROP PROCEDURE IF EXISTS `insertproduct`;
delimiter ;;
CREATE PROCEDURE `insertproduct`(in `idproduct` int,in `productname` varchar(255), in `price` float(50,3))
BEGIN

 INSERT INTO product VALUES(idproduct,productname,price);

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for insertsale
-- ----------------------------
DROP PROCEDURE IF EXISTS `insertsale`;
delimiter ;;
CREATE PROCEDURE `insertsale`(in `idsale` int,in `idproduct` int,in `amount` int,in `total` float)
BEGIN

 INSERT INTO sale VALUES(idsale,idproduct,amount,total);

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
