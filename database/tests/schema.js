import mysql from 'mysql';

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'admin',
    password : 'pass',
    database : 'billapp'
});
connection.connect();

describe('Database schema', function() {
    it('bill table', function(done) {
        const query = 'SELECT DISTINCT COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'bill\';';
        connection.query(query, (error, results) => {

            expect(results.length).toBe(3);

            expect(results[0].COLUMN_NAME).toBe('IdBill');
            expect(results[0].ORDINAL_POSITION).toBe(1);
            expect(results[0].DATA_TYPE).toBe('int');
            expect(results[0].IS_NULLABLE).toBe('NO');

            expect(results[1].COLUMN_NAME).toBe('IdClient');
            expect(results[1].ORDINAL_POSITION).toBe(2);
            expect(results[1].DATA_TYPE).toBe('int');
            expect(results[1].IS_NULLABLE).toBe('NO');

            expect(results[2].COLUMN_NAME).toBe('Dateissued');
            expect(results[2].ORDINAL_POSITION).toBe(3);
            expect(results[2].DATA_TYPE).toBe('datetime');
            expect(results[2].IS_NULLABLE).toBe('NO');
            done();
        });
    });


    it('client table', function(done) {
        const query = 'SELECT DISTINCT COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, IS_NULLABLE, CHARACTER_MAXIMUN_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'client_user\';';
        connection.query(query, (error, results) => {
            expect(results.length).toBe(2);

            expect(results[0].COLUMN_NAME).toBe('RUC');
            expect(results[0].ORDINAL_POSITION).toBe(1);
            expect(results[0].DATA_TYPE).toBe('int');
            expect(results[0].IS_NULLABLE).toBe('NO');

            expect(results[1].COLUMN_NAME).toBe('Name');
            expect(results[1].ORDINAL_POSITION).toBe(2);
            expect(results[1].DATA_TYPE).toBe('varchar');
            expect(results[0].CHARACTER_MAXIMUM_LENGTH).toBe(100);
            expect(results[1].IS_NULLABLE).toBe('NO');
            done();
        });
    });

    it('detail table', function(done) {
        const query = 'SELECT DISTINCT COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'detail\';';
        connection.query(query, (error, results) => {
            expect(results.length).toBe(2);

            expect(results[0].COLUMN_NAME).toBe('IdBill');
            expect(results[0].ORDINAL_POSITION).toBe(1);
            expect(results[0].DATA_TYPE).toBe('int');
            expect(results[0].IS_NULLABLE).toBe('NO');

            expect(results[1].COLUMN_NAME).toBe('Idsales');
            expect(results[1].ORDINAL_POSITION).toBe(2);
            expect(results[1].DATA_TYPE).toBe('int');
            expect(results[1].IS_NULLABLE).toBe('NO');
            done();
        });
    });

    it('product table', function(done) {
        const query = 'SELECT DISTINCT COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, IS_NULLABLE, CHARACTER_MAXIMUN_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'product\';';
        connection.query(query, (error, results) => {
            expect(results.length).toBe(3);

            expect(results[0].COLUMN_NAME).toBe('IdProduct');
            expect(results[0].ORDINAL_POSITION).toBe(1);
            expect(results[0].DATA_TYPE).toBe('int');
            expect(results[0].IS_NULLABLE).toBe('NO');

            expect(results[1].COLUMN_NAME).toBe('Name');
            expect(results[1].ORDINAL_POSITION).toBe(2);
            expect(results[1].DATA_TYPE).toBe('varchar');
            expect(results[0].CHARACTER_MAXIMUM_LENGTH).toBe(100);          
            expect(results[1].IS_NULLABLE).toBe('NO');

            expect(results[2].COLUMN_NAME).toBe('Price');
            expect(results[2].ORDINAL_POSITION).toBe(3);
            expect(results[2].DATA_TYPE).toBe('float');
            expect(results[2].IS_NULLABLE).toBe('NO');
            done();
        });
    });

    it('sale table', function(done) {
        const query = 'SELECT DISTINCT COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = \'sale\';';
        connection.query(query, (error, results) => {
            expect(results.length).toBe(4);

            expect(results[0].COLUMN_NAME).toBe('IdSale');
            expect(results[0].ORDINAL_POSITION).toBe(1);
            expect(results[0].DATA_TYPE).toBe('int');
            expect(results[0].IS_NULLABLE).toBe('NO');

            expect(results[1].COLUMN_NAME).toBe('IdProcut');
            expect(results[1].ORDINAL_POSITION).toBe(2);
            expect(results[1].DATA_TYPE).toBe('int');
            expect(results[1].IS_NULLABLE).toBe('NO');

            expect(results[2].COLUMN_NAME).toBe('Amount');
            expect(results[2].ORDINAL_POSITION).toBe(3);
            expect(results[2].DATA_TYPE).toBe('int');
            expect(results[2].IS_NULLABLE).toBe('NO');

            expect(results[3].COLUMN_NAME).toBe('Total');
            expect(results[3].ORDINAL_POSITION).toBe(4);
            expect(results[3].DATA_TYPE).toBe('float');
            expect(results[3].IS_NULLABLE).toBe('NO');
            done();
        });
    });
});
